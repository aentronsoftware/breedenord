// New 07.04.2021 Old 11.03.2021
// EMUS - BreedeNord

/// 182 Master and 282 Slave Base Id Adress

//////////////////////////////////////////
//#include <CAN.h>
#include "ESP32SJA1000.h"
ESP32SJA1000Class CANESP;
#define ON  LOW
#define OFF HIGH

#define SD_MOSI      18
#define SD_MISO      19
#define SD_SCK       5
#define SD_CS_PIN   14

int LED_R = 2;
int LED_B = 4;
int LED_G = 15;

const int TemperatureSize = 121;
const int VoltageSize = 239;

float Charge_Temp[TemperatureSize] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 80.64, 80.64, 80.64, 80.64, 80.64, 80.64, 80.64, 80.64,
                          80.64, 80.64, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140, 140,
                          140, 140, 140, 140, 140, 140, 140, 140, 126, 112, 98, 84, 70, 56, 42, 28, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                         };

int TemperatureDerating [TemperatureSize] = { -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24, -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8,
                                  -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
                                  35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
                                  74, 75, 76, 77, 78, 79, 80
                                };

float Discharge_Tempe[TemperatureSize] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 231, 231, 231, 231, 231, 231, 231, 231, 231,
                              231, 234, 242, 250, 258, 266, 274, 282, 290, 298, 306, 314, 322, 330, 338, 346, 354, 362, 370, 378, 386, 394, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400,
                              400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 360, 320, 280, 240, 200, 160, 120, 80,
                              40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                             };

float Discharge_Volt[VoltageSize] = {400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 ,
                             400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 ,
                             400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 ,
                             400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 ,
                             400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 ,
                             400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 400 , 388.71 , 370.2 , 351.69 , 333.18 , 314.67 , 296.16 , 277.65 , 259.14 , 240.63 , 222.12 , 203.61 ,
                             185.1 , 166.59 , 148.08 , 129.57 , 111.06 , 92.55 , 74.04 , 55.53 , 37.02 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
                             0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0
                            };

float Voltage_Derating[VoltageSize] = {764.40 , 763.10 , 761.80 , 760.50 , 759.20 , 757.90 , 756.60 , 755.30 , 754.00 , 752.70 , 751.40 , 750.10 , 748.80 , 747.50 , 746.20 , 744.90 , 743.60 , 742.30 , 741.00 , 739.70 , 738.40 ,
                               737.10 , 735.80 , 734.50 , 733.20 , 731.90 , 730.60 , 729.30 , 728.00 , 726.70 , 725.40 , 724.10 , 722.80 , 721.50 , 720.20 , 718.90 , 717.60 , 716.30 , 715.00 , 713.70 , 712.40 , 711.10 ,
                               709.80 , 708.50 , 707.20 , 705.90 , 704.60 , 703.30 , 702.00 , 700.70 , 699.40 , 698.10 , 696.80 , 695.50 , 694.20 , 692.90 , 691.60 , 690.30 , 689.00 , 687.70 , 686.40 , 685.10 , 683.80 ,
                               682.50 , 681.20 , 679.90 , 678.60 , 677.30 , 676.00 , 674.70 , 673.40 , 672.10 , 670.80 , 669.50 , 668.20 , 666.90 , 665.60 , 664.30 , 663.00 , 661.70 , 660.40 , 659.10 , 657.80 , 656.50 ,
                               655.20 , 653.90 , 652.60 , 651.30 , 650.00 , 648.70 , 647.40 , 646.10 , 644.80 , 643.50 , 642.20 , 640.90 , 639.60 , 638.30 , 637.00 , 635.70 , 634.40 , 633.10 , 631.80 , 630.50 , 629.20 ,
                               627.90 , 626.60 , 625.30 , 624.00 , 622.70 , 621.40 , 620.10 , 618.80 , 617.50 , 616.20 , 614.90 , 613.60 , 612.30 , 611.00 , 609.70 , 608.40 , 607.10 , 605.80 , 604.50 , 603.20 , 601.90 ,
                               600.60 , 599.30 , 598.00 , 596.70 , 595.40 , 594.10 , 592.80 , 591.50 , 590.20 , 588.90 , 587.60 , 586.30 , 585.00 , 583.70 , 582.40 , 581.10 , 579.80 , 578.50 , 577.20 , 575.90 , 574.60 ,
                               573.30 , 572.00 , 570.70 , 569.40 , 568.10 , 566.80 , 565.50 , 564.20 , 562.90 , 561.60 , 560.30 , 559.00 , 557.70 , 556.40 , 555.10 , 553.80 , 552.50 , 551.20 , 549.90 , 548.60 , 547.30 ,
                               546.00 , 544.70 , 543.40 , 542.10 , 540.80 , 539.50 , 538.20 , 536.90 , 535.60 , 534.30 , 533.00 , 531.70 , 530.40 , 529.10 , 527.80 , 526.50 , 525.20 , 523.90 , 522.60 , 521.30 , 520.00 ,
                               518.70 , 517.40 , 516.10 , 514.80 , 513.50 , 512.20 , 510.90 , 509.60 , 508.30 , 507.00 , 505.70 , 504.40 , 503.10 , 501.80 , 500.50 , 499.20 , 497.90 , 496.60 , 495.30 , 494.00 , 492.70 ,
                               491.40 , 490.10 , 488.80 , 487.50 , 486.20 , 484.90 , 483.60 , 482.30 , 481.00 , 479.70 , 478.40 , 477.10 , 475.80 , 474.50 , 473.20 , 471.90 , 470.60 , 469.30 , 468.00 , 466.70 , 465.40 ,
                               464.10 , 462.80 , 461.50 , 460.20 , 458.90 , 457.60 , 456.30 , 455.00
                              };

const unsigned long eventIntervalCANbus = 500;
const unsigned long eventIntervalSerial = 10000;
const unsigned long eventIntervalCharger = 1500;
const unsigned long eventIntervalCommu = 5000;  /// 1000ms

unsigned long previousTime1 = 0, previousTime2 = 0, previousTime3 = 0, previousTime4 = 0, previousTime5 = 0;

int SOC_Master, SOC_Slave, SOH_Master_Slave, OutSig_Master, OutSig_Slave, CellMin_Master, CellMax_Master, CellMin_Slave, CellMax_Slave, BattVolt_Master, BattVolt_Slave,
    BattCurr_Master, BattCurr_Slave, AverTemp_Master_Slave, TempMin_Master, TempMax_Master, TempMin_Slave, TempMax_Slave, Errors_Master, Warnings_Master, Errors_Slave, Warnings_Slave = 0;

int BattSOC, Output_signal = 0, minCell = 0, maxCell = 0, BattVoltage = 0, BattCurrent = 0, minTemp = 0, maxTemp = 0, DeltaCell = 0, CanMintemp = 0, CanMaxtemp = 0;

int CellDeltaR = 100; ///200mv error, 100mV warning
int ChargeCurrentLimit = 0; /// Charge current Scaling 0.1A
int Dischrage_Current = 0;

// array to store binary number
byte binaryNum[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
byte WarndataSlave[8];
byte WarndataMaster[8];

byte ErrordataMaster1[8];
byte ErrordataSlave1[8];
byte ErrordataMaster6[8];
byte ErrordataSlave6[8];
byte WarndataMaster5[8];
byte WarndataSlave5[8];
byte WarndataMaster4[8];
byte WarndataSlave4[8];
int Errors_Master1, Errors_Master2, Errors_Slave1, Errors_Slave2, Errors_Master6, Errors_Slave6;
byte ErrordataMaster2[8];
byte ErrordataSlave2[8];

byte Outsig_Master[8];
byte Outsig_Master1[8];
byte Outsig_Slave[8];

byte errorbit0, errorbit1, errorbit2, errorbit3;
byte warnbit4, warnbit5, warnbit6, warnbit7;
int Batterystatus = 5;
unsigned long CounterResetNode = 0;

int Reset_HighTemp_Current = 0, Reset_WHighTemp_Current = 0, Reset_WLowTemp_Current = 0, Reset_LowTemp_Current = 0;
int PackDiff = 0, PackError = 0, PackCounter = 0;
int MasterCANbus = 0, SlaveCANbus = 0, MasterCANError = 1, SlaveCANError = 1;

//////////////////////////
byte data18A[8], data28A[8];
byte data182[8], data282[8];
byte data187[8], data287[8];
byte data189[8], data289[8];
byte data183[8], data283[8];
byte data181[8], data581[8];
byte data281[8], data681[8];
byte data381[8], data781[8];
byte data481[8], data881[8];
byte data601[8], data65E[8];

bool SDOReceived = false;

/////////////////////////////////////
void setup()
{
  delay(3000);
  Serial.begin(9600);
  //////////////////////////////////////////
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, OFF);
  digitalWrite(LED_G, OFF);
  digitalWrite(LED_R, OFF);
  //////////////////////////////////////
  //Serial.println("CAN Receiver Callback");
  // register the receive callback
  CANESP.setPins(26, 25); /// CANbus
  CANESP.begin(250E3);    /// 250 kbits
  CANESP.onReceive(onReceive);
  delay(3000);
  while (!Serial);
}
/////////////////////////////////////////
void loop() {
  unsigned long currentTime = millis();
  /* This is the event */
  if (currentTime - previousTime5 >= eventIntervalCommu) {
    /* Event code */
    CANComm();
    /* Update the timing for the next time around */
    previousTime5 = currentTime;
  }
  /* This is the event */
  if (currentTime - previousTime1 >= eventIntervalSerial) {
    /* Event code */
    // SerialDebug();
    /* Update the timing for the next time around */
    previousTime1 = currentTime;
  }

  /* This is the event */
  if (currentTime - previousTime2 >= eventIntervalCANbus) {
    /* Event code */
    RelayControl();
    /* Update the timing for the next time around */
    previousTime2 = currentTime;

    //////////////////////
    if (PackDiff >= 10) {
      PackCounter++;
      if (PackCounter > 10) {
        PackError = 1;
      }
    } else {
      PackError = 0;
      PackCounter = 0;
    }
    //////////////////////
    /*  if ((Outsig_Slave[5] == 1) || (Outsig_Master[5] == 1)) {
        }
        else {
          PackError = 0;
          PackCounter = 0;
        }*/
  }

  /* This is the event */
  if (currentTime - previousTime3 >= eventIntervalCharger) {
    /* Event code */
    if (Outsig_Master1[2] == 1) ChargerControl();
    /* Update the timing for the next time around */

    previousTime3 = currentTime;
  }
  ///-----------------------------------------
  float ChargCurrent_Temp = PermittedCharge_Temp(TemperatureDerating, CanMaxtemp, TemperatureSize);
  float DischCurrent_Temp = PermittedDischarg_Temp(TemperatureDerating, CanMaxtemp, TemperatureSize);
  float DischCurrent_Volt = PermittedDischarg_Volt(Voltage_Derating, BattVoltage, VoltageSize); 

  if (DischCurrent_Temp <= DischCurrent_Volt) {
    Dischrage_Current = (DischCurrent_Temp)*10.0;
  } else {
    DischCurrent_Temp = (DischCurrent_Volt)*10.0;
  }

  ChargeCurrentLimit = (ChargCurrent_Temp)*10.0;
  ///----------------------------------------
  ////////////////////////////////
  /// Real Calculation
  BattSOC = Average_Calculate(data187[6], data287[6]); /// Master and Slave average SOC

  decToBinary(data182[1]);
  for (int j = 7; j >= 0; j--)  {
    Outsig_Master[7 - j] = binaryNum[j];
  }
  decToBinary(data282[1]);
  for (int j = 7; j >= 0; j--)  {
    Outsig_Slave[7 - j] = binaryNum[j];
  }
  //  Serial.println(" ");
  for (int j = 7; j >= 0; j--)  {
    Outsig_Master1[7 - j] = (Outsig_Master[j] | Outsig_Slave[j]);
  }
  Output_signal = 0;
  for (int j = 0; j <= 7; j++)  {
    Output_signal = (pow(2, j) * Outsig_Master1[j]) + Output_signal;
  }

  minCell = Calculate_Min(data183[0], data283[0]);
  maxCell = Calculate_Max(data183[1], data283[1]);

  BattVolt_Master = (data183[5] << 24 | data183[6] << 16 | data183[3] << 8 | data183[4]); //total Voltage 3-4-5-6
  BattVolt_Slave = (data283[5] << 24 | data283[6] << 16 | data283[3] << 8 | data283[4]);  //total Voltage  3-4-5-6
  BattVoltage = Calculate_Max(BattVolt_Master, BattVolt_Slave) * 0.1;
  PackDiff = abs((BattVolt_Master * 0.01) - (BattVolt_Slave * 0.01));
  //Serial.println(PackDiff);
  //---------------------------------
  String MSB1 = String(data187[1], HEX);
  String LSB1 = String(data187[0], HEX);
  if ((data187[0]) == 0xFF)  {
    if (data187[1] <= 0xF)  {
      MSB1 = "0" + MSB1;
    }
  }
  signed int BattCurr_Master = (int16_t)(strtol((LSB1 + MSB1).c_str(), NULL, 16));
  //---------------------------------
  String MSB2 = String(data287[1], HEX);
  String LSB2 = String(data287[0], HEX);
  if ((data287[0]) == 0xFF)  {
    if (data287[1] <= 0xF)  {
      MSB2 = "0" + MSB2;
    }
  }
  signed int BattCurr_Slave = (int16_t)(strtol((LSB2 + MSB2).c_str(), NULL, 16));
  //------------------------------------
  // BattCurr_Master = (data187[0] << 8 | data187[1]); //total Current 0-1
  // BattCurr_Slave = (data287[0] << 8 | data287[1]); //total Current 0-1
  BattCurrent = (BattCurr_Master + BattCurr_Slave);

  minTemp = Calculate_Min(data18A[0], data28A[0]) - 100;
  if (minTemp == -100) minTemp = 0;
  maxTemp = Calculate_Max(data18A[1], data28A[1]) - 100;
  if (maxTemp == -100) maxTemp = 0;

  CanMintemp = Calculate_Min(data18A[0], data28A[0]);
  CanMaxtemp = Calculate_Max(data18A[1], data28A[1]);

  AverTemp_Master_Slave = Average_Calculate(maxTemp, minTemp) * 10; /// Master and Slave average Temperature
  ///----------------------------------------------------
  Errors_Master1 = data189[0];
  Errors_Master2 = data189[2];

  decToBinary(Errors_Master1);
  for (int j = 7; j >= 0; j--)   {
    ErrordataMaster1[7 - j] = binaryNum[j];
  }
  decToBinary(Errors_Master2);
  for (int j = 7; j >= 0; j--)  {
    ErrordataMaster2[7 - j] = binaryNum[j];
  }
  ///------------------------------------------------------
  Errors_Slave1 = data289[0];
  Errors_Slave2 = data289[2];

  decToBinary(Errors_Slave1);
  for (int j = 7; j >= 0; j--)  {
    ErrordataSlave1[7 - j] = binaryNum[j];
  }
  decToBinary(Errors_Slave2);
  for (int j = 7; j >= 0; j--)  {
    ErrordataSlave2[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster1[i] = ErrordataMaster1[i] | ErrordataSlave1[i];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster2[i] = ErrordataMaster2[i] | ErrordataSlave2[i];
  }
  ///------------------------------------------------------
  Errors_Master6 = data189[6];
  Errors_Slave6  = data289[6];
  decToBinary(data289[6]);
  for (int j = 7; j >= 0; j--)  {
    ErrordataSlave6[7 - j] = binaryNum[j];
  }
  decToBinary(data189[6]);
  for (int j = 7; j >= 0; j--)  {
    ErrordataMaster6[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    ErrordataMaster6[i] = ErrordataMaster6[i] | ErrordataSlave6[i];
  }
  ///------------------------------------------------------
  decToBinary(data289[5]);
  for (int j = 7; j >= 0; j--)  {
    WarndataSlave5[7 - j] = binaryNum[j];
  }
  decToBinary(data189[5]);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster5[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster5[i] = WarndataMaster5[i] | WarndataSlave5[i];
  }
  ///------------------------------------------------------
  decToBinary(data289[4]);
  for (int j = 7; j >= 0; j--)  {
    WarndataSlave4[7 - j] = binaryNum[j];
  }
  decToBinary(data189[4]);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster4[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster4[i] = WarndataMaster4[i] | WarndataSlave4[i];
  }
  ///----------------------------------------------------
  errorbit0 = 0b10101010;
  errorbit1 = 0b10101010;
  errorbit2 = 0b10101010;
  errorbit3 = 0b10101010;
  byte errorbit = 0x00000000;

  if (ErrordataMaster1[7] == 1)  {
    errorbit0 = errorbit0 & 0b11011111;
    errorbit0 = errorbit0 | 0b00010000; /// Bit 0: Under-voltage
    //   Serial.println("Undervoltage");
  }
  if (ErrordataMaster1[6] == 1) {
    errorbit0 = errorbit0 & 0b11110111;
    errorbit0 = errorbit0 | 0b00000100; /// Bit 1: Over-voltage
    //  Serial.println("Over Voltage");
  }
  if (ErrordataMaster1[5] == 1) {
    errorbit1 = errorbit1 & 0b01111111;
    errorbit1 = errorbit1 | 0b01000000; /// Bit 2: Discharge Over-current
    //  Serial.println("Discharge Over-current");
  }
  if (ErrordataMaster1[4] == 1) {
    errorbit2 = errorbit2 & 0b11111101;
    errorbit2 = errorbit2 | 0b00000001; /// Bit 3: Charge Over-current
    //  Serial.println("Charge Over-current");
  }
  if (ErrordataMaster1[3] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 4: Cell Module Overheat
    //  Serial.println("BMS Internal- Cell Module Overheat");
  }
  if ((ErrordataMaster1[1] == 1) || (SlaveCANError == 1) || (MasterCANError == 1) || (PackError == 1)) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 6: No Cell Communication
    //   Serial.println("BMS Internal- No Cell Comm");
  }
  if (ErrordataMaster2[0] == 1)  {
    errorbit1 = errorbit1 & 0b11111101;
    errorbit1 = errorbit1 | 0b00000001; /// Bit 1: Low Temperature
    //  Serial.println("Cell Underheat");
    if (Reset_LowTemp_Current == 1)
    {
      errorbit1 = errorbit1 & 0b11011111;
      errorbit1 = errorbit1 | 0b00010000; /// Bit 2: battery temperature is lower than the battery accepts for charging
    }
    if ((BattCurrent * 0.1) > 3) {
      errorbit1 = errorbit1 & 0b11011111;
      errorbit1 = errorbit1 | 0b00010000; /// Bit 2: battery temperature is lower than the battery accepts for charging
      //   Serial.println("charging Module underheat");
      Reset_LowTemp_Current = 1;
    }
  }  else
  {
    Reset_LowTemp_Current = 0;
  }
  if (ErrordataMaster2[4] == 1)  {
    errorbit0 = errorbit0 & 0b01111111;
    errorbit0 = errorbit0 | 0b01000000; /// Bit 1: Low Temperature
    //  Serial.println("Cell Over-Heat");
    if (Reset_HighTemp_Current == 1)    {
      errorbit1 = errorbit1 & 0b11110111;
      errorbit1 = errorbit1 | 0b00000100; /// Bit 2: battery temperature is lower than the battery accepts for charging
    }
    if ((BattCurrent * 0.1) > 3) {
      errorbit1 = errorbit1 & 0b11110111;
      errorbit1 = errorbit1 | 0b00000100; /// Bit 2: battery temperature is lower than the battery accepts for charging
      //    Serial.println("charging Module Over-Heat");
      Reset_HighTemp_Current = 1;
    }
  }
  else  {
    Reset_HighTemp_Current = 0;
  }
  if (ErrordataMaster2[3] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 6: No current Sensor No Cell Communication -- imbalance between cells
    //  Serial.println("BMS Internal,LEM");
  }
  if (ErrordataMaster2[2] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 0: PackUnder-voltage
    // Serial.println("BMS- internal Pack Undervoltage");
  }
  if (ErrordataMaster2[1] == 1) {
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 1: PackUnder-voltage
    // Serial.println("BMS Internal Pack Over-Voltage");
  }
  if (ErrordataMaster6[7] == 1) {
    errorbit3 = errorbit3 & 0b11111101; /// Good
    errorbit3 = errorbit3 | 0b00000001; /// Bit 1: PackUnder-voltage
    //  Serial.println("Cell Voltage deviation");
  }
  if (ErrordataMaster6[22] == 11) { ///?
    errorbit2 = errorbit2 & 0b01111111;
    errorbit2 = errorbit2 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("BMS Pack Volt Deviation");
  }
  ///---------------------------------------------
  Warnings_Master = data189[1];
  decToBinary(Warnings_Master);
  for (int j = 7; j >= 0; j--)  {
    WarndataMaster[7 - j] = binaryNum[j];
  }
  Warnings_Slave = data289[1];
  decToBinary(Warnings_Slave);
  for (int j = 7; j >= 0; j--)  {
    WarndataSlave[7 - j] = binaryNum[j];
  }
  for (int i = 0; i <= 7; i++)  {
    WarndataMaster[i] = WarndataMaster[i] | WarndataSlave[i];
  }

  //----------------------------------------------
  warnbit4 = 0b10101010;
  warnbit5 = 0b10101010;
  warnbit6 = 0b10101010;
  warnbit7 = 0b10101010;

  if (WarndataMaster[7] == 1) {
    warnbit4 = warnbit4 & 0b11011111;;
    warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //   Serial.println("Wlow Cell voltage");
  }
  if (WarndataMaster[6] == 1) {
    warnbit5 = warnbit5 & 0b01111111;;
    warnbit5 = warnbit5 | 0b01000000; /// Bit 1: High current – discharge current (negative current) exceeds the current warning setting.
    //  Serial.println("Whigh dichar current");
  }
  if (WarndataMaster[5] == 1) {
    warnbit6 = warnbit6 & 0b01111111;;
    warnbit6 = warnbit6 | 0b01000000; /// Bit 2: High temperature – cell module temperature exceeds warning temperature setting
    //  Serial.println("WBMS high Cell Module temp");
  }
  //-------------------------------------
  if (WarndataMaster4[7] == 1) {
    warnbit4 = warnbit4 & 0b11011111;;
    warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WlCell under-volt");
  }
  if (WarndataMaster4[6] == 1) {
    warnbit4 = warnbit4 & 0b11110111;;
    warnbit4 = warnbit4 | 0b00000100; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    // Serial.println("W Cell Over-Volt");
  }
  /* if (WarndataMaster[7] == 1) {
     warnbit4 = warnbit4 & 0b11011111;;
     warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
     Serial.println("WPack Under-Volt");
    }*/
  /* if (WarndataMaster[7] == 1) {
     warnbit4 = warnbit4 & 0b11011111;;
     warnbit4 = warnbit4 | 0b00010000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
     Serial.println("WPack Over-Volt");
    }*/
  if (WarndataMaster4[5] == 1) {
    warnbit5 = warnbit5 & 0b01111111;;
    warnbit5 = warnbit5 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    // Serial.println("WDisch Over-Curr");
  }
  if (WarndataMaster4[4] == 1) {
    warnbit6 = warnbit6 & 0b11111101;;
    warnbit6 = warnbit6 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("W Charge Over curr");
  }
  if (WarndataMaster4[2] == 1) {
    warnbit4 = warnbit4 & 0b01111111;;
    warnbit4 = warnbit4 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WCell Over-heat");
    if (Reset_WHighTemp_Current == 1) {
      warnbit5 = warnbit5 & 0b11110111;;
      warnbit5 = warnbit5 | 0b00000100; /// Bit 2: High temperature – charge
    }
    if ((BattCurrent * 0.1) > 3) {
      warnbit5 = warnbit5 & 0b11110111;;
      warnbit5 = warnbit5 | 0b00000100; /// Bit 2: High temperature – charge
      //   Serial.println("Wcharge high temp");
      Reset_WHighTemp_Current = 1;
    }
  }
  else  {
    Reset_WHighTemp_Current = 0;
  }
  if (WarndataMaster4[0] == 1) {
    warnbit5 = warnbit5 & 0b11111101;;
    warnbit5 = warnbit5 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("W Cell under-heat");
    if (Reset_WLowTemp_Current == 1) {
      warnbit5 = warnbit5 & 0b11011111;;
      warnbit5 = warnbit5 | 0b00010000; /// Bit 2: High temperature – charge
    }
    if ((BattCurrent * 0.1) > 3) {
      warnbit5 = warnbit5 & 0b11011111;;
      warnbit5 = warnbit5 | 0b00010000; /// Bit 2: High temperature – charge
      //    Serial.println("Wcharge Low temp");
      Reset_WLowTemp_Current = 1;
    }
  }
  else {
    Reset_WLowTemp_Current = 0;
  }
  if (WarndataMaster5[2] == 1) {
    warnbit6 = warnbit6 & 0b01111111;;
    warnbit6 = warnbit6 | 0b01000000; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WBMS Cell Comm Loss");
  }
  if (WarndataMaster5[7] == 1) {
    warnbit7 = warnbit7 & 0b11111101;;
    warnbit7 = warnbit7 | 0b00000001; /// Bit 0: Low voltage – some cell is below low voltage warning setting.
    //  Serial.println("WLow Cell Devi");
  }
  /* if ((BattVoltage * 0.01) > 700)
    {
     warnbit6 = warnbit6 & 0b01111111;;
     warnbit6 = warnbit6 | 0b01000000; /// Bit 0: high voltage – some cell is below low voltage warning setting.
     // Serial.println("WBMSPack Volt Devi");
    }*/

  /*if ((BattCurrent * 0.1) > 130) {
    warnbit6 = warnbit6 & 0b11111101;;
    warnbit6 = warnbit6 | 0b00000001; /// Bit 2: LOW temperature – charge
    Serial.println("Wcharge high");
    }*/

  /* if ((data189[4] == 0x80) || (data289[4] == 0x80)) {
     warnbit5 = warnbit5 & 0b11111101;;
     warnbit5 = warnbit5 | 0b00000001; /// Bit 2: LOW temperature – cell module temperature exceeds warning temperature setting
     // Serial.println("WLow Tempe warning");

     if ((BattCurrent * 0.1) > 3) {
       warnbit5 = warnbit5 & 0b11011111;;
       warnbit5 = warnbit5 | 0b00010000; /// Bit 2: LOW temperature – charge
       //    Serial.println("Wcharge low temp");
     }
    }*/
  /* if (DeltaCell > (CellDeltaR / 2))  {
     warnbit7 = warnbit7 & 0b11111101;;
     warnbit7 = warnbit7 | 0b00000001; /// Bit 2: LOW temperature – cell module temperature exceeds warning temperature setting
     Serial.println("WLow cell delta");
    }*/
}
///---------------------------------------------------------------
void RelayControl()
{
  if (Errors_Slave1 > 0 || Errors_Slave2 > 0 || Errors_Master1 > 0 || Errors_Master2 > 0 || Errors_Master6 > 0 || Errors_Slave6 > 0
      || MasterCANError == 1 || SlaveCANError == 1 || PackError == 1)
  {
    CANESP.beginPacket(0x19E);
    CANESP.write(0);
    CANESP.endPacket();
    CANESP.beginPacket(0x29E);
    CANESP.write(0);
    CANESP.endPacket();
  }
  else
  {
    if (data601[0] == 1)
    {
      CANESP.beginPacket(0x19E);
      CANESP.write(1);
      CANESP.endPacket();
      CANESP.beginPacket(0x29E);
      CANESP.write(1);
      CANESP.endPacket();
    }
    else
    {
      CANESP.beginPacket(0x19E);
      CANESP.write(0);
      CANESP.endPacket();
      CANESP.beginPacket(0x29E);
      CANESP.write(0);
      CANESP.endPacket();
    }
  }
  //  --------------------------------------------------------------
  CANESP.beginPacket(0x30); /// demo version 0x1DE
  CANESP.write(data181[0]);
  CANESP.write(data181[1]); /// Voltag Charge  * 0.1
  CANESP.write(ChargeCurrentLimit & 0xFF);
  CANESP.write(ChargeCurrentLimit >> 8); /// Charge Current * 0.1
  CANESP.write(Dischrage_Current & 0xFF);
  CANESP.write(Dischrage_Current >> 8); /// Disch Current  * 0.1
  CANESP.write(data181[6]);
  CANESP.write(data181[7]); /// Disch Voltage  * 0.1
  CANESP.endPacket();

  CANESP.beginPacket(0x40); /// demo version 0x2DE
  CANESP.write(BattSOC);
  CANESP.write(0);
  CANESP.write(data281[2]);/// SoH from Master
  CANESP.write(0);
  CANESP.write(Output_signal);
  CANESP.write(minCell);
  CANESP.write(maxCell);
  CANESP.write(0);
  CANESP.endPacket();

  CANESP.beginPacket(0x50); /// demo version 0x3DE
  CANESP.write(BattVoltage & 0xFF);
  CANESP.write(BattVoltage >> 8);
  CANESP.write(BattCurrent & 0xFF);
  CANESP.write(BattCurrent >> 8);
  CANESP.write(AverTemp_Master_Slave & 0xFF);
  CANESP.write(AverTemp_Master_Slave >> 8);
  CANESP.write(CanMintemp);
  CANESP.write(CanMaxtemp);
  CANESP.endPacket();

  CANESP.beginPacket(0x60); /// demo version 0x4DE
  CANESP.write(errorbit0);
  CANESP.write(errorbit1);
  CANESP.write(errorbit2);
  CANESP.write(errorbit3);
  CANESP.write(warnbit4);
  CANESP.write(warnbit5);
  CANESP.write(warnbit6);
  CANESP.write(warnbit7);
  CANESP.endPacket();
  ///-----------------------------------
  CANESP.beginPacket(0x75E);
  CANESP.write(Batterystatus);
  CANESP.endPacket();

  if (SDOReceived)  {
    CANESP.beginPacket(0x5DE);
    CANESP.write(0x42);
    CANESP.write(0x18);
    CANESP.write(0x10);
    CANESP.write(0x04);
    CANESP.write(0xF5);
    CANESP.write(0x43);
    CANESP.write(0x70);
    CANESP.write(0x00);
    CANESP.endPacket();
    SDOReceived =  false;
  }
}
///-------------------------------------
void onReceive(int packet)  {
  for (int i = 0; i < 8; i++)  {
    if (CANESP.available())    {
      if (CANESP.packetId() == 0x1A5)       {
        data18A[i] = CANESP.read();
        LEDstatusG();       MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x1A3)  {
        data182[i] = CANESP.read();
        LEDstatusG();       MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x1A2)  {
        data187[i] = CANESP.read();
        LEDstatusG();       MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x1A6)  {
        data189[i] = CANESP.read();
        LEDstatusG();       MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x1A4)  {
        data183[i] = CANESP.read();
        LEDstatusG();       MasterCANbus++;
      }
      else if (CANESP.packetId() == 0x1A8)  {
        data283[i] = CANESP.read();
        LEDstatusG();       SlaveCANbus++;
      }
      else if (CANESP.packetId() == 0x1A1)  {
        data28A[i] = CANESP.read();
        LEDstatusG();       SlaveCANbus++;
      }
      else if (CANESP.packetId() == 0x1A7)  {
        data282[i] = CANESP.read();
        LEDstatusG();       SlaveCANbus++;
      }
      else if (CANESP.packetId() == 0x1A9)  {
        data287[i] = CANESP.read();
        LEDstatusG();       SlaveCANbus++;
      }
      else if (CANESP.packetId() == 0x1AA)  {
        data289[i] = CANESP.read();
        LEDstatusG();        SlaveCANbus++;
      }
      else if (CANESP.packetId() == 0x181) {
        data181[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x281) {
        data281[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x581) {
        data601[i] = CANESP.read();
        LEDstatusG();
      }
      else if (CANESP.packetId() == 0x65E) {
        data65E[i] = CANESP.read();
        SDOReceived = true;
        LEDstatusG();
      }
      else {
        //digitalWrite(LED_R, ON);
        //digitalWrite(LED_G, OFF);
      }
    }
    else   {
      /*
        // Serial.println("data Received times Max 20");
        // Serial.print(counterdatamissing);
        counterdatamissing++;
        if (counterdatamissing > 20)
        {
         Serial.println("No data Received");
         digitalWrite(LED_R, ON);
         digitalWrite(LED_G, OFF);
        }*/
    }
  }
}

void LEDstatusG() {
  digitalWrite(LED_G, ON);
  digitalWrite(LED_R, OFF);
}
///////////////////////////
int Average_Calculate(int MasterData, int SlaveData) {
  int result = (MasterData + SlaveData) / 2;
  return result;
}
///////////////////////////
int Calculate_OR(int MasterData, int SlaveData) {
  int result = (MasterData | SlaveData);
  return result;
}
///////////////////////////
int Calculate_Min(int MasterData, int SlaveData) {
  int result = SlaveData;
  if (MasterData <= SlaveData)  result = MasterData;
  return result;
}
///////////////////////////
int Calculate_Max(int MasterData, int SlaveData) {
  int result = SlaveData;
  if (MasterData >= SlaveData)  result = MasterData;
  return result;
}
///////////////////////////
void ChargerControl() {
  CANESP.beginPacket(0x306);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.write(0);
  CANESP.endPacket();
}
///////////////////////////
void SerialDebug() {
  Serial.print("SOC ");  Serial.print(BattSOC); Serial.print(" ");
  Serial.print("SOH ");  Serial.print(SOH_Master_Slave); Serial.print(" ");
  Serial.print("Output_signal ");  Serial.print(Output_signal); Serial.print(" ");
  Serial.print("minCell ");  Serial.print(minCell * 2 * 0.01); Serial.print(" ");
  Serial.print("maxCell ");  Serial.print(maxCell * 2 * 0.01); Serial.println(" ");
  Serial.print("DeltaCell ");  Serial.print(DeltaCell * 2 * 0.01); Serial.print(" ");
  Serial.print("BattVoltage ");  Serial.print(BattVoltage); Serial.print(" ");
  Serial.print("BattCurrent ");  Serial.print(BattCurrent * 0.1); Serial.print(" ");
  Serial.print("AverTemp_Master_Slave ");  Serial.print(AverTemp_Master_Slave * 0.1); Serial.print(" ");
  Serial.print("minTemp ");  Serial.print(minTemp); Serial.print(" ");
  Serial.print("maxTemp ");  Serial.print(maxTemp); Serial.println(" ");
}
////////////////////////////////////////////
float PermittedCharge_Temp(int Tempe[], int TempretureV, int SizeArr)
{
  int Position = LookUp1D(Tempe, TempretureV, SizeArr);
  if ((Position >= 0) && (Position <= SizeArr))  {
    return Charge_Temp[Position];
  }
  else  {
    return 0.0;
  }
}
////////////////////////////////////////////
float PermittedDischarg_Temp(int Tempe[], int TempretureV, int SizeArr)
{
  int Position = LookUp1D(Tempe, TempretureV, SizeArr);
  if ((Position >= 0) && (Position <= SizeArr))  {
    return Discharge_Tempe[Position];
  }
  else  {
    return 0.0;
  }
}
////////////////////////////////////////////
////////////////////////////////////////////
float PermittedDischarg_Volt(float Voltage_Derating1[], float VoltV, int SizeArr)
{
  int Position = LookUp2D(Voltage_Derating1, VoltV, SizeArr);
  if ((Position >= 0) && (Position <= SizeArr))  {
    return Discharge_Volt[Position];
  }
  else  {
    return 0.0;
  }
}
////////////////////////////////////////////
///----------------------------------------
int LookUp1D(int TempeV[], int LookTemp, int Length)
{
  for (int i = 0; i < Length; i++)  {
    if (LookTemp == TempeV[i]) {
      return i;
    }
  }
  return -1;
}
///----------------------------------------
int LookUp2D(float VoltagV[], float LookVolt, int Length)
{
  for (int i = 0; i < Length - 1; i++)  {
    if (LookVolt <= VoltagV[i]) {
      if (LookVolt > VoltagV[i + 1]) {
        return i + 1;
      }
    }
  }
  return -1;
}
///---------------------------------------
//////////////////////////////////////////
void CANComm() {
  if  (SlaveCANbus > 2) {
    SlaveCANbus = 0; SlaveCANError = 0;
  } else {
    SlaveCANError = 1;
  }
  if (MasterCANbus > 2) {
    MasterCANbus = 0; MasterCANError = 0; ;
  } else {
    MasterCANError = 1;
  }
}
////////////////////////////////////////
// function to convert decimal to binary
void decToBinary(int n) {
  for (int j = 0; j <= 8; j++) {
    binaryNum[j] = 0;
  }
  if (n != 0)  {
    // counter for binary array
    int i = 0;
    while (n > 0) {
      // storing remainder in binary array
      binaryNum[i] = n % 2;
      n = n / 2;
      i++;
    }
  }
  else  {
    for (int k = 0; k <= 8; k++)    {
      binaryNum[k] = 0;
    }
  }
}
